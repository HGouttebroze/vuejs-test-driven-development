# Install Docker

## Comment lancer simplement un serveur web nginx

`docker run -d -p 8080:80 --name web nginx`

### Les commandes de base

sudo

docker ps
docker ps -a
docker run -tid -p 8080:80 -v /srv/data/nginx/:/usr/share/nginx/html/ --name web nginx:latest
docker stop web
docker start web
docker rm -f web

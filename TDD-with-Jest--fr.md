# TESTS

## TDD (Test Driven Developement)

- Développement piloté par les tests:
  - principe de développement logiciel qui garantit que votre code fonctionne exactement comme vous le souhaitez
  - principe qui devrait être suivi par chaque développement
  - développement piloté par les tests
    - puis suivre le développement ultérieur du débogage

* Pourquoi suivre le principe de développement piloté par les tests ?

  - pour facilite le débogage :
    - avec une approche de développement pilotée par les tests, en cas de bug ou problème en base, les tests échoueront certainement
    - ce qui permet de savoir exactement où chercher, quoi rechercher et quoi modifier. cela facilite le développement, debugage, sécurité, qualité ...

* Le TDD (Test Driven Development) vous aide et garantit que le code se comporte comme attendu.

### Diffétents types de développement piloté par les tests

- Test de l'unité

  - Le test unitaire est un type de test de développement logiciel où les composants individuels/unitaires d'un logiciel sont testés. Le processus est effectué pendant le développement d'une application, pour assurer et vérifier l'exactitude d'une unité/composant de code.

- Test d'intégration

  - Le test d'intégration est le processus de test de la collection/groupe d'unité/composant. Le test d'intégration est effectué à l'issue des tests unitaires

- Test d'acceptation/E2E (de bout en bout)

  - Les tests de bout en bout sont une méthodologie utilisée pour tester si le flux d'une application fonctionne comme prévu du début à la fin. Le but des tests de bout en bout est d'identifier les dépendances du système et de s'assurer que les bonnes informations sont transmises entre les divers composants et systèmes du système.

### Cycle de développement piloté par les tests

- Tout comme il y a 3 cycles derrière le contrôle des feux rouges, jaunes et verts, il y a 3 cycles derrière le développement piloté par les tests

  - Red,
  - Green
  - Refactor

  1. Rouge : écrivez un test et il échoue
  2. Vert : écrivez un test pour une fonctionnalité et assurez-vous qu'elle passe le test
  3. Refactor : Optimiser le test passé précédent et s'assurer que tous les cas de test réussissent.

### Qu'est-ce que Jest ?

- Jest est un framework de test JavaScript axé sur la simplicité.
  Il fonctionne avec des projets utilisant : Babel , TypeScript , Node , React , Angular , Vue ...

- Comment installer `Jest` dans votre projet Javascript

`yarn add --dev jest`

`npm install --save-dev jest`

- Mot-clé `TDD` pour `Jest`

  - `expect` : lorsque vous écrivez des tests, vous devez souvent vérifier que les valeurs remplissent certaines conditions. `expect` donne accès à un certain nombre de "matchers" qui vous permettent de valider différentes choses. expect accepter une valeur (une valeur que vous souhaitez vérifier par rapport à ce que vous attendez).

  ```js
  attendre(somme(1,2)).toBe(3);
  attendre(somme(1,2)).toBeA(nombre);
  attendre(somme(1,2).toEqual(3);
  la fonction expect a un matcher que vous pouvez utiliser et enchaîner avec en plaisantant. tels que .toBe(), toBeA , toEqual, toContainEqualetc. consultez la documentation officielle pour plus de correspondances attendues.
  test/it :test ou itest une fonction qui exécute le test, elle vous permet de tester votre fonction unitaire par exemple. Disons qu'il existe une fonction appelée sumqui accepte deux paramètres et renvoie l'addition des paramètres.
  test('Somme de deux nombres', () => {
  expect(Sum(5,9)).toBe(14); }
  );
  ```

# Démarrer avec Jest en utilisant Javascript

- initialiser le projet javascript avec npm en utilisant votre terminal

`npm init --y`

- installez `jest` dans votre projet à l'aide du terminal

`npm install --save-dev jest`

- Pour créer un fichier de test avec une `.test.js` extension qui permettra à `jest` de localiser votre fichier et d'exécuter vos cas de test. Pour organiser et structurer votre projet, vous pouvez créer un dossier avec le nom **test** dans votre répertoire de projet qui abritera tous les fichiers de cas de test.

      - Exemple :

          - Supposons que l'on vous confie la tâche d'écrire une fonction qui accepte deux valeurs en tant que paramètre et additionne les deux valeurs.
              - Il faut maintenant savoir ce quoi tester.
              - Il y a beaucoup de choses à tester.

          - Réfléchir au type de `sortie/résultat` à renvoyer à l'utilisateur :

              - Testez si la valeur des paramètres est de type de données `number`, s'il ne s'agit pas d'un nombre, le test devrait échouer.

              - Testez si la somme du paramètre est logiquement correcte, sinon le test devrait échouer (`failed`).

          - ARRAY : Spour qu'un paramètre accepte un tableau et additionne également ce tableau, écrire un scénario de test qui :
              - vérifie si l'utilisateur fournit un tableau en tant que paramètre
              - vérifie s'il renvoie une réponse correcte

        - Présentons 2 solution différente en appliquant le `TDD` :

            - Deux façon différentes `TDD` soit vous écrivez vos cas de test avant de commencer réellement votre développement, soit vous écrivez les cas de test plus tard après le développement, mais la meilleure pratique est d'abord le cas de test, puis le développement. c'est l'approche que nous allons suivre maintenant.

            - Créons un dossier appelé **test** au niveau racine de notre projet, à l'intérieur du dossier créez un fichier appelé `sum.test.js`, tous nos cas de test pour la fonction somme seront dans le fichier.

  - Dans le cas d'utilisation de `commonJS`, il faut configurer le projet pour utiliser `babel`

  ```js
  import somme
  const { somme } = require("./somme");
  describe('Ajout du test de fonctionnalité à deux nombres',()=>{
  test("Ajouter 1 + 2 devrait être 3", () => {
  attendre(somme(1, 2)).toBe(3);
  attendre(somme(1,2)).toBeA(nombre);
  });
  test("il devrait échouer si chaîne en paramètre", () => {
  expect(sum("Bonjour", "Monde"))
  .toEqual(Error("Type de nombre attendu comme paramètre"));
  });
  });
  ```

  - Explication :

    - Le premier cas de test vérifiera si le premier paramètre et la deuxième valeur du paramètre renvoient une réponse correcte. c'est-à-dire si `1+2` égal à `3`

      ```
      test("Ajouter 1+2 devrait être 3", () => {
          attendre(somme(1, 2)).toBe(3);
      });
      ```

      - Le deuxième cas de test vérifiera si le premier paramètre ou la deuxième valeur de paramètre est de type `String` et renverra une erreur

      ```js
      test("il devrait échouer si chaîne en paramètre", () => {
          expect(sum("Bonjour", "Monde"))
          .toEqual(Error("Type de nombre attendu comme paramètre"))
      };
      ```

    * après avoir écrit les tests, il faut s'assurer que la fonction `sum` réussit dans tous les cas de test.

      - Créer un fichier appelé `sum.js `dans le dossier racine
      - le fichier contiendra une fonction qui accepte deux nombres et qui le additionne

        ```js
        somme const = (a, b) => {
            if (typeof a !== "numbre" || typeof b !== "numbre")
                return Error("Type de numbre attendu comme paramètre");
            return a + b;
        };
        ```

        ```js
        module.exports = { somme };
        ```

        - extrait ci-dessus vérifie d'abord:
          - le type de données du paramètre s'il ne s'agit pas d'un nombre, il devrait renvoyer une erreur avec le message Type `numbre` attendu comme paramètre

    - Execution du test :

      - configurer le projet pour qu'il fonctionne avec `jest`

      `package.json`, add `“test”: “jest”` comme `script.`, soit :

      ```json
          "scripts": {
          "test": "blague"
          },
      ```

      - execution du test :
        `yarn test`

    * résultat/sortie de notre test

      `Tous les cas de test réussis`

### Coomment écrire un test

- test écrits pour connexion
- il existe de nombreuses façons d'écrire un test

  - réfléchir ce qu'il faut vérifier

  ```js
  describe("Module de connexion pour les cas de test", () => {
      it("devrait retourner l'utilisateur si le nom est valide", function(done) {
          demande (application)
                  .post("/connexion")
                  .send({ nom : "DAMMAK", mot de passe : "Adedamola" })
                  .end(function(err, res) {
                      expect(res.statusCode).toEqual(200);
                      expect(res.text).toEqual(
                          JSON.stringify({ nom : "DAMMAK", mot de passe : "Adedamola" })
                          );
                          terminé();
                  });
              });
              it("échoue si les informations de connexion sont erronées", function(done) {
                  demande (application)
                      .post("/connexion")
                      .send({ nom : "wrongUser", motdepasse : "wrongPass" })
                      .end(function(err, res) {
                      expect(res.statusCode).toEqual(400);
                  terminé();
                  });
              });
              it("échoue si des données de requête vides ont été envoyées", function(done) {
                  demande (application)
                      .post("/connexion")
                      .envoyer({})
                      .end(function(err, res) {
                      expect(res.statusCode).toEqual(400);
                  terminé();
          });
      });
  });
  ```
